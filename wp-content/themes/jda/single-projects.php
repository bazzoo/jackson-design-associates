<?php get_header();


    $filePath = __DIR__ . DIRECTORY_SEPARATOR . 'sliders' . DIRECTORY_SEPARATOR. 'slider_export.txt';
    $data = unserialize( file_get_contents( $filePath ) );

    $upload_dir = wp_upload_dir();
    $fileString .= $upload_dir['basedir'] . DIRECTORY_SEPARATOR;
    $fileString .= $post->post_title  . '-?.jpg';
    $glob = str_replace(' ', '-', $fileString);

    $slides[] = $data['slides'];

    $i=0;
    foreach (glob($glob) as $filename)
    {
        $slides[$i]                      = $data['slides'][0];
        $slides[$i]['id']                = $i;
        $slides[$i]['params']['image']   = $filename;

        $i++;
    }

    //get array of images that have been uploaded
    //add each one as a slide

    //var_dump($slides);

    putRevSlider($post->post_title );
?>

    <?php while (have_posts()) : the_post(); ?>

    <div class="row full-width info-row">
        <div class="large-4 medium-6 small-12 columns info-container">
            <article>
                <h4><?php the_title(); ?>
                    <button aria-label="Close"> </button>
                </h4>
                <?php if (has_excerpt($post->ID)) { echo '<h2>' . the_excerpt() . '</h2>'; } ?>

                <ul>
                    <li class="title">Client</li>
                    <li><?php echo get_post_meta($post->ID, '_client', true) ?></li>
                    <?php echo wpdocs_custom_taxonomies_terms_links(); ?>
                </ul>

                <div class="writeup">
                    <?php the_content(); ?>
                </div>
            </article>
        </div>

        <div class="large-8 medium-6 small-12 columns"> </div>

    <footer>
        <?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'foundationpress'), 'after' => '</p></nav>')); ?>
    </footer>


    <?php endwhile; ?>

<?php do_action('foundationpress_after_content'); ?>

<?php get_footer(); ?>
