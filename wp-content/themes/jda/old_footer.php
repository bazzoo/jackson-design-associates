</section>
<div class="row full-width diff footer">
    <a id="contact"></a>

    <div class="row"">
    <div class="small-12 medium-12 large-12 columns">

        <div class="small-12 medium-4 large-4 columns">

            <h3>Follow Us</h3>
            <p class="text-center">For our latest work and news.</p>
            <p class="text-center"><i class="fa fa-linkedin-square big"></i>
                <i class="fa fa-pinterest-square big"></i></p>
        </div>





        <div class="small-12 medium-4 large-4 columns">
            <h3>Newsletter</h3>
            <p class="text-center"></p>
            <div class="large-12 columns">
                <input type="text" placeholder="Name">
            </div>
            <div class="large-12 columns">
                <div class="row collapse">
                    <div class="small-10 columns">
                        <input type="text" placeholder="Email Address">
                    </div>
                    <div class="small-2 columns">
                        <a href="#" class="button postfix">Go</a>
                    </div>
                </div>
            </div>
        </div>




        <div class="small-12 medium-4 large-4 columns">
            <h3>Contact Us</h3>
            <h2 class="text-center">01234 567 890</h2>
            <center><a class="button">Contact Form</a></center>
        </div>
    </div>
</div>

<?php do_action( 'foundationpress_before_footer' ); ?>

<?php do_action( 'foundationpress_after_footer' ); ?>


<a class="exit-off-canvas"></a>

<?php do_action( 'foundationpress_layout_end' ); ?>
</div>
</div>
<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script>
    new WOW().init();
</script>

<script type="text/javascript">
    function scrollWin()
    {
        var y = jQuery(window).scrollTop(); jQuery("html, body").animate({ scrollTop: y + 750 }, 600);
    }
</script>

</div></body>
</html>