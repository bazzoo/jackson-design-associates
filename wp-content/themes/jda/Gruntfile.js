module.exports = function (grunt)
{
    require('time-grunt')(grunt);

    grunt.initConfig
    ({
        pkg: grunt.file.readJSON('package.json'),


        //grunt.registerTask('build', ['copy', 'string-replace:fontawesome', 'sass', 'concat', 'uglify']);
        //                              ****
        copy:
        {
            scripts:
            {
                expand: true,
                cwd: 'bower_components/foundation/js/vendor/',
                src: '**',
                flatten: 'true',
                dest: 'js/vendor/'
            },

            iconfonts:
            {
                expand: true,
                cwd: 'bower_components/fontawesome/',
                src: ['**', '!**/less/**', '!**/css/**', '!bower.json'],
                dest: 'assets/fontawesome/'
            }
        },


        //grunt.registerTask('build', ['copy', 'string-replace:fontawesome', 'sass', 'concat', 'uglify']);
        //                                      **************************
        'string-replace':
        {
            fontawesome:
            {
                files:
                {
                    'assets/fontawesome/scss/_variables.scss': 'assets/fontawesome/scss/_variables.scss'
                },
                options:
                {
                    replacements:
                        [
                            {
                                pattern: '../fonts',
                                replacement: '../assets/fontawesome/fonts'
                            }
                        ]
                }
            }
        },



        //grunt.registerTask('build', ['copy', 'string-replace:fontawesome', 'sass', 'concat', 'uglify']);
        //                                                                    ****
        sass:
        {
            options:
            {
                // If you can't get source maps to work, run the following command in your terminal:
                // $ sass scss/foundation.scss:css/foundation.css --sourcemap
                // (see this link for details: http://thesassway.com/intermediate/using-source-maps-with-sass )
                // sourceMap: true
            },

            dist:
            {
                options:
                {
                    outputStyle: 'compressed'
                },
                files:
                {
                    'css/foundation.css': 'scss/foundation.scss'
                }
            }
        },


        //grunt.registerTask('build', ['copy', 'string-replace:fontawesome', 'sass', 'concat', 'uglify']);
        //                                                                            ******
        concat:
        {
            options:
            {
                separator: ';'
            },

            dist:
            {
                src:
                    [
                        // Foundation core
                        'bower_components/foundation/js/foundation/foundation.js',

                        // Pick the componenets you need in your project
                        'bower_components/foundation/js/foundation/foundation.abide.js',
                        'bower_components/foundation/js/foundation/foundation.accordion.js',
                        'bower_components/foundation/js/foundation/foundation.alert.js',
                        'bower_components/foundation/js/foundation/foundation.clearing.js',
                        'bower_components/foundation/js/foundation/foundation.dropdown.js',
                        'bower_components/foundation/js/foundation/foundation.equalizer.js',
                        'bower_components/foundation/js/foundation/foundation.interchange.js',
                        'bower_components/foundation/js/foundation/foundation.joyride.js',
                        'bower_components/foundation/js/foundation/foundation.magellan.js',
                        'bower_components/foundation/js/foundation/foundation.offcanvas.js',
                        'bower_components/foundation/js/foundation/foundation.orbit.js',
                        'bower_components/foundation/js/foundation/foundation.reveal.js',
                        'bower_components/foundation/js/foundation/foundation.slider.js',
                        'bower_components/foundation/js/foundation/foundation.tab.js',
                        'bower_components/foundation/js/foundation/foundation.tooltip.js',
                        'bower_components/foundation/js/foundation/foundation.topbar.js',
                        'bower_components/jquery-ui/jquery-ui.js',
                        'bower_components/jquery-validation/dist/jquery.validate.js',

                        'node_modules/jquery-smooth-scroll/jquery.smooth-scroll.js',
                        'node_modules/waypoints/lib/jquery.waypoints.js',
                        'node_modules/waypoints/lib/shortcuts/inview.js',
                        'node_modules/jquery.flip/src/jquery.flip.js',
                        'node_modules/isotope-layout/dist/isotope.pkgd.js',
                        'node_modules/isotope-packery/packery-mode.pkgd.js',

                        // Include your own custom scripts (located in the custom folder)
                        //'js/custom/_waypoints.js',
                        'js/custom/bazzoo.js',
                    ],

                // Finally, concatenate all the files above into one single file
                dest: 'js/foundation.js'
            }
        },


        //grunt.registerTask('build', ['copy', 'string-replace:fontawesome', 'sass', 'concat', 'uglify']);
        //                                                                                      ******
        uglify:
        {
            dist:
            {
                files:
                {
                    // Shrink the file size by removing spaces
                    'js/foundation.js': ['js/foundation.js']
                }
            }
        },



        //grunt.registerTask('default', ['browserSync', 'watch']);
        //                                    2-1
        browserSync:
        {
            dev:
            {
                bsFiles:
                {
                    src: ['css/foundation.css', 'js/foundation.js']
                },
                options:
                {
                    proxy: "jda.bzo-staging.co.uk"
                }
            }
        },



        //grunt.registerTask('default', ['browserSync', 'watch']);
        //                                                2-2
        watch:
        {
            grunt: {files: ['Gruntfile.js']},

            js:
            {
                files: 'js/**/*.js',
                tasks: ['concat'],
                options:
                {
                    livereload: true,
                }
            },

            sass:
            {
                files: 'scss/**/*.scss',
                tasks: ['sass'],
                options:
                {
                    livereload: true,
                }
            },

            all:
            {
                files: '**/*.php',
                options:
                {
                    livereload: true,
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-browser-sync');


  //grunt.registerTask('build', ['copy', 'string-replace:fontawesome', 'sass', 'concat', 'uglify']);
  //                               1                   2                 3         4         5
    grunt.registerTask('build', ['copy', 'string-replace:fontawesome', 'sass', 'concat', 'uglify']);

  //grunt.registerTask('default', ['browserSync', 'watch']);
  //                                    2-1          2-2
    grunt.registerTask('default', ['browserSync:dev']);


};

