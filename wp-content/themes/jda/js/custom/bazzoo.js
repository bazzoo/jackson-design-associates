$ = jQuery;

var currentPage = '';

if ($('body').hasClass('page-template-projects-pinterest'))
{
    currentPage = 'projects';
}

else if ($('body').hasClass('page-template-people'))
{
    currentPage = 'people';
}

else if ($('body').hasClass('single-projects'))
{
    currentPage = 'single-projects';
}

else if ($('body').hasClass('page-template-news'))
{
    currentPage = 'news';
}

else
{
    currentPage = 'home';
}

function setGridSizes()
{
    var gs = $('.grid-sizer');
    var wi = gs.width();
    gs.height(wi);
    $('.grid-item').css({'height': wi, 'width': wi});
    $('.grid-item--width2').css({'width': wi * 2});
    $('.grid-item--height2').css({'height': wi * 2});
}

function infoContainer_toggle(direction)
{
    if ($('.info-container button').hasClass('spun')) // Back in (not out!)
    {
        $('.info-container').animate({'left': "0"});
        $('.info-container button').toggleClass('spun');
        $('.info-container h4').toggleClass('poked');
    }

    else //out (not back in!)
    {
        $('.info-container').animate({'left': "-33vw"});
        $('.info-container h4').toggleClass('poked');
        $('.info-container button').toggleClass('spun');
    }
}

function isiPhone()
{
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/))
    {
        return true;
    }
    else
    {
        return false;
    }
}

$(document).ready(function ()
{
    var wayPoints = new Array();

    if (currentPage == 'home')
    {
        setGridSizes();

        if (isiPhone() == true)
        {
            $('.grid-item').addClass('grid-item--big');
            $('.grid-item').addClass('ios');
        }
        else
        {
            var $dc = $('.disciplines-container');

            $dc.isotope
            ({
                animationEngine: 'best-available',
                itemSelector: '.grid-item',
                layoutMode: 'masonry',
                percentPositioning: false,
                transitionDuration: '0.8s',
                masonry: {
                    isFitWidth: true,
                    columnWidth: 284,
                    gutter: 21
                },
                packery: {
                    gutter: 21
                }
            });

            $dc.on('click', '.grid-item:not(.grid-item--big)', function ()
            {
                // change size of item via class
                $(this).toggleClass('grid-item--big');
                $(this).siblings().removeClass('grid-item--big');
                $('.disciplines-container').isotope();
            });

            $dc.on('click', '.grid-item--big button', function ()
            {
                //console.log ($(this).parents().eq(3));
                // change size of item via class
                $(this).parents().eq(3).toggleClass('grid-item--big');
                $(this).parents().eq(3).siblings().removeClass('grid-item--big');
                $('.disciplines-container').isotope();
            });
        }

        $('a').each(function ()
        {
            if (this.id !== typeof undefined && this.id !== false && this.id !== "" && this.id !== "nav")
            {
                var os  = $(this).offset();
                var id  = $(this).attr('id');
                var num = os.top;

                wayPoints.push({"id": id, "num": num});
                console.log(wayPoints);
            }
        })

        var whatAreWe = new Waypoint
        ({
            element: $('.what-are-we')[0],
            offset: 160,
            handler: function ()
            {
                $('.what-are-we .header').toggleClass('fixed');
            }
        });


        var quotationOne = new Waypoint
        ({
            element: $('.quotation.one')[0],
            offset: 350,
            handler: function ()
            {
                $('.what-are-we .header').toggleClass('faded');
            }
        });

        var ourEthos = new Waypoint
        ({
            element: $('.what-we-do')[0],
            offset: 160,
            handler: function ()
            {
                $('.what-we-do .header').toggleClass('fixed');
            }
        });

        var quotationTwo = new Waypoint
        ({
            element: $('.quotation.two')[0],
            offset: 350,
            handler: function ()
            {
                $('.what-we-do .header').toggleClass('faded');
            }
        });
    }

    if (currentPage == 'single-projects')
    {
        $('.info-container button').on('click', function ()
        {
            infoContainer_toggle()
        });

        var singlePageWaypoint = new Waypoint
        ({
            element: $('body.single div.rev_slider_wrapper'),
            handler: function (direction)
            {
                if (direction == "down")
                {
                    $('.info-container').animate({'bottom': '400px'}, 50)
                    $('div.tp-bullets.dione.vertical.noSwipe').addClass('absolute')
                }
                if (direction == "up")
                {
                    $('.info-container').animate({'bottom': '20px'}, 50)
                    $('div.tp-bullets.dione.vertical.noSwipe').removeClass('absolute')
                }
            }
        });

    }


    if (currentPage == 'projects')
    {
        $ = jQuery;
        setGridSizes()
        $('.projects-container').isotope
        ({
            animationEngine: 'best-available',
            itemSelector: '.grid-item',
            layoutMode: 'packery',
            //percentPosition: true,
            transitionDuration: '0.8s',

            masonry: {
                isFitWidth: false,
                columnWidth: '.grid-sizer',
                gutter: 0
            },

            packery: {
                gutter: 0
            }
        });

        // bind filter button click
        $('.filters-button-group').on('click', 'button', function ()
        {
            var filterValue = $(this).attr('data-filter');
            $('.projects-container').isotope({filter: filterValue});
        });


        // change is-checked class on buttons
        $('.button-group').each(function (i, buttonGroup)
        {
            var $buttonGroup = $(buttonGroup);
            $buttonGroup.on('click', 'button', function ()
            {
                $buttonGroup.find('.is-checked').removeClass('is-checked');
                $(this).addClass('is-checked');
            });
        });
    }


    $('#nav').on('click', function ()
    {
        var here = $(document).scrollTop();
        $.each(wayPoints, function (i)
        {
            var there = wayPoints[i]['num'] - 160;

            console.log('here = ' + here + ' : ');
            if (here < there)
            {
                $('html, body').animate({scrollTop: there});
                return false;
            }

            if (here == there)
            {
                var there = wayPoints[i + 1]['num'];
                $('html, body').animate({scrollTop: there - 160});
                return false;
            }

            if ($('#nav').hasClass('bottom'))
            {
                $('html, body').animate({scrollTop: 0});
                return false;
            }
        })
    });

    var iv5 = new Waypoint.Inview
    ({
        element: $('.footer')[0],
        enter: function (direction)
        {
            $('.next-button > div').addClass('bottom');
        },
        exit: function (direction)
        {
            $('.next-button > div').removeClass('bottom');
        }
    })


    jQuery.validator.addMethod('answercheck', function (value, element)
    {
        return this.optional(element) || /^\bcat\b$/.test(value);
    }, "type the correct answer -_-");


    $('#contact-us').on('click', function ()
    {
        // change size of item via class
        $('#contact-form').toggleClass('show');
    });

    $('#contact-form > a > button').on('click', function ()
    {
        // change size of item via class
        $('#contact-form').toggleClass('show');
    });

    // validate contact form
    $(function ()
    {
        $('#contact').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true
                },
                answer: {
                    required: true,
                    answercheck: true
                }
            },
            messages: {
                name: {
                    required: "come on, you have a name don't you?",
                    minlength: "your name must consist of at least 2 characters"
                },
                email: {
                    required: "no email, no message"
                },
                message: {
                    required: "um...yea, you have to write something to send this form.",
                    minlength: "thats all? really?"
                },
                answer: {
                    required: "sorry, wrong answer!"
                }
            },
            submitHandler: function (form)
            {
                $(form).ajaxSubmit({
                    type: "POST",
                    data: $(form).serialize(),
                    url: "process.php",
                    success: function ()
                    {
                        $('#contact :input').attr('disabled', 'disabled');
                        $('#contact').fadeTo("slow", 0.15, function ()
                        {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor', 'default');
                            $('#success').fadeIn();
                        });
                    },
                    error: function ()
                    {
                        $('#contact').fadeTo("slow", 0.15, function ()
                        {
                            $('#error').fadeIn();
                        });
                    }
                });
            }
        });
    });

});


$(document).foundation();

