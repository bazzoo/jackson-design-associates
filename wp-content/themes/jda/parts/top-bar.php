
<a class="next-button visible">
    <div id="nav" class="button next back"></div>
</a>


<div class="top-bar-container contain-to-grid sticky show-for-large-up<?php if (is_page_template('page-home.php') != true) { echo ' fixed';} ?>">
    <div class="logo"></div>

    <nav class="top-bar" data-topbar role="navigation">

        <a href="/">
            <h1>Jackson Design Associates</h1>
            <ul class="title-area">
                <li>Architecture</li>
                <li>Design</li>
                <li>Management</li>
            </ul>
        </a>

        <section class="top-bar-section">
            <?php foundationpress_top_bar_l(); ?>
            <?php foundationpress_top_bar_r(); ?>
        </section>

    </nav>
    <div class="underneath"></div>
</div>
