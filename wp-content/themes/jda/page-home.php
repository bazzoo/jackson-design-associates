<?php
/*
Template Name: Homepage
*/
get_header(); ?>

	<?php /* Start loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="home-content">
				<?php
                    the_content();
                    include('disciplines.php');
                ?>
			</div>
			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
			</footer>
		</article>
	<?php endwhile; // End the loop ?>

<?php get_footer(); ?>
