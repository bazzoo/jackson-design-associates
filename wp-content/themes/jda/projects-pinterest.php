<?php
/**
 * Template Name: Pinterest view
 **/

    get_header();
    $disciplines = get_terms('discipline');
    $sectors    = get_terms('sector');

?>

<div class="row full-width">
    <div class="isotope">
        <div class="button-group filters-button-group">
            <button class="button" data-filter="*">All</button>
            <?php
            foreach($disciplines as $discipline)
            {
                $button  = '<button class="button" data-filter=".';
                $button .=  $discipline->slug;
                $button .= '">';
                $button .= $discipline->name;
                $button .= '</button>';

                echo $button;
            }
            ?>
        </div>

        <div class="button-group filters-button-group">
            <button class="button" data-filter="*">All</button>
            <?php
            foreach($sectors as $sector)
            {
                $button  = '<button class="button" data-filter=".';
                $button .=  $sector->slug;
                $button .= '">';
                $button .= $sector->name;
                $button .= '</button>';

                echo $button;
            }
            ?>
        </div>


        <div class="small-12 medium-12 large-12 columns projects-container">
            <div class="grid-sizer"></div>

            <?php

                $args = array();
                $args['post_type']      = 'projects';
                $args['post_status']    = 'publish';
                $args['posts_per_page'] = '-1';

                $loop = new WP_Query( $args );
                if ( $loop->have_posts() )
                {
                    while ( $loop->have_posts() )
                    {
                        $loop->the_post();
                        $meta = get_post_meta($post->ID);

                        $html  =  '            <a href="' . $post->guid . '">' . "\n";

                        if (has_post_thumbnail( $post->ID ) )
                        {
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');

                            $html .=  '               <div style="background-image:url(' . $image[0] . ');" class="grid-item';
                        }

                        $disciplines = wp_get_post_terms($post->ID, 'discipline');

                        foreach ($disciplines as $discipline)
                        {
                            $html .= ' ' . $discipline->slug;
                        }

                        switch ($meta['_size'][0])
                        {
                            case 'portrait':
                                $html .= ' grid-item--width2';
                                break;

                            case 'large':
                                $html .= ' grid-item--width2  grid-item--height2';
                                break;
                        }

                        $sectors = wp_get_post_terms($post->ID, 'sector');
                        foreach ($sectors as $sector)
                        {
                            $html .= ' ' . $sector->slug;
                        }

                        $html .= '">' . "\n" . '                   <div class="frame">' . "\n";

                        $html .= '                       <div>' . "\n" . '                           <h2 class="wow fadeInUp">'.$post->post_title.'</h2>' . "\n";

                        if ($post->post_excerpt == '')
                        {
                            $post->post_excerpt = 'JDA to provide this excerpt';
                        }

                        $html .= '                           <p>'.$post->post_excerpt.'</p>' . "\n";

                        $html .= '                       </div>'. "\n";
                        $html .= '                   </div>'. "\n";
                        $html .= '               </div>'. "\n";
                        $html .= '           </a>'. "\n";

                        echo $html;
                    }
                }
                wp_reset_postdata(); // reset to the original page data
                wp_reset_query(); // reset to the original page data
            ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>