<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bzostagi_jda');

/** MySQL database username */
define('DB_USER', 'bzostagi_jda');

/** MySQL database password */
define('DB_PASSWORD', 'mysql5249ooz');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Added to coree by Wes @ bazzoo to allow .svg uploads. */
define('ALLOW_UNFILTERED_UPLOADS', true);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ASa`G]y>%??Er{6HyCAFQ|x_G;fw:Ff5](Q49u:t3<fZX;h]a?JEGj)FR[?H17,K');
define('SECURE_AUTH_KEY',  'IGde--lj#!G|P*<{nyQrqCAcnLk[9+MtbH(MW8;[C0Pn+Z5#S3#PGDoG1eK*xs!%');
define('LOGGED_IN_KEY',    '(p&VZiq&:P^EEAx+^8bmfmq`Y+ggpr@0d[B~8K0^j^:F{9`*>YQKb#`rG)2%>,vN');
define('NONCE_KEY',        '*[-q/WJjnH?E+RFRN_1Q?6j|P!]krPl{j+!IN-J/D.Y &|n4op[ctE4OgNVnep&V');
define('AUTH_SALT',        'Lj1[HND`S-Y-t5@|nyVr;Go{uL|1=hyX[@I_i9o+#G6.)L@.P(Ecql$UEvsWvU8(');
define('SECURE_AUTH_SALT', '?ds0Xn1IJ#$q NNfXk3g{_Tq0S0;mn8!DIm_Fz-.>Fd|_pK 0?=sH^.D{{D-1WMU');
define('LOGGED_IN_SALT',   'Og#]xabIfkEdg=bZ)/%-/T+S&P:C-cHydxm&5MsZZDSG-qw4{-z-|K 9O!OUY+)U');
define('NONCE_SALT',       'aIV.`Pzi-<2IP`FX[n`8avv/e3c8|+10T+E-d+ZZ3`@@gn-V` wh}OB#L} eqn:2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
