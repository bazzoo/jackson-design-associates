</section>

</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->

<div class="row full-width diff footer" id="bottom">

    <div class="small-3 small-offset-1 medium-4 large-2 large-offset-2 columns follow" id="follow">
        <p>Follow JDA</p>
        <ul>
            <li><i class="fa fa-linkedin big"></i></li>
            <li><i class="fa fa-pinterest big"></i></li>
            <li><i class="fa fa-twitter big"></i></li>
        </ul>
    </div>


    <div class="small-6 small-offset-1 medium-4 large-2 end large-offset-4 columns contact end">
        <p id="contact-us">Contact us</p>
        <ul>
            <li>01623 863 222</li>
            <li><a href="mailto:mail@jacksondesign.co.uk">mail@jacksondesign.co.uk</a></li>
            <li>Latimer House</li>
            <li>Newark, Nottinghamshire</li>
            <li>NG22 9QW</li>
        </ul>
    </div>
</div>

<div id="contact-form">
    <button class="wow fadeInRight">×</button>
    <form id="contact" name="contact" method="post">
        <fieldset>

            <label for="name">Name<span class="required">*</span></label>
            <input type="text" name="name" id="name" size="30" value="" required/>

            <label for="email">Email<span class="required">*</span></label>
            <input type="text" name="email" id="email" size="30" value="" required/>

            <label for="phone">Phone</label>
            <input type="text" name="phone" id="phone" size="30" value="" />

            <label for="message">Message<span class="required">*</span></label>
            <textarea name="message" id="message" required></textarea>

            <label for="captcha">Name the small house pet that says "<i>meow</i>"<span class="required">*</span></label>
            <input type="text" name="captcha" id="captcha" value="" required/>

            <input id="submit" type="submit" name="submit" value="Send" />

        </fieldset>
    </form>

    <div id="success">
        <p><span>Your message was sent succssfully. We will be in touch as soon as possible.</span></p>
    </div>

    <div id="error">
        <p>
            <span>Something went wrong, try refreshing and submitting the form again.</span></p>
    </div>
</div>


<?php do_action('foundationpress_before_footer'); ?>
<?php do_action('foundationpress_after_footer'); ?>

<a class="exit-off-canvas"></a>

<?php do_action('foundationpress_layout_end'); ?>
<?php wp_footer(); ?>
<?php do_action('foundationpress_before_closing_body'); ?>

<script src="//cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js?id=1234"></script>

<script type="text/javascript">
    new WOW().init();

    function scrollWin()
    {
        var y = jQuery(window).scrollTop();
        jQuery("html, body").animate({scrollTop: y + 750}, 600);
    }
</script>
</body>
</html>