<?php
/**
 * Template Name: Discipline Boxes
 **/


$args = array();
$args['post_type'] = 'discipline_boxes';
$args['post_status'] = 'publish';
$args['posts_per_page'] = '4';
$args['orderby'] = 'ID';
$args['order'] = 'asc';

$boxes = new WP_Query($args);

$html  = '<div class="row disciplines-row">';
$html .= '<div class="disciplines-container small-12 medium-12 large-12">';

while ($boxes->have_posts())
{
    $boxes->the_post();
    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');

    $html .= '<div class="grid-item wow fadeInUp '.$post->post_name.'" data-wow-delay="0.4s">';
    $html .= '    <div class="content">';
    $html .= '        <div class="trans">';
    $html .= '            <div class="container">';
    $html .= '                <button aria-label="Close">&times;</button>';
    $html .= '                <h4>' . $post->post_title . '</h4>';
    $html .= '                <p>Key skills within this discipline:</p>';
    $html .= '                ' . $post->post_content;
    $html .= '                <p>Projects that have required these skills:</p>';
    $html .= '                <ul>';

    $args = array();
    $args['post_type']      = 'projects';
    $args['post_status']    = 'publish';
    $args['posts_per_page'] = '4';
    $args['orderby']        = 'rand';

    $projects = new WP_Query($args);

    if($projects->have_posts())
    {
        foreach ($projects->posts as $project)
        {
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($project->ID), 'thumbnail');
            $link  = ($project->guid);

            $html .= '                    <li>';
            $html .= '                         <figure>';
            $html .= '                         <a href="'. $link .'"><figure>';
            $html .= '                             <img src="' . $image[0] . '" />';
            $html .= ' 							<figcaption>' . $project->post_title . '</figcaption>';
            $html .= '                         </figure></a>';
            $html .= '                         </figure>';
            $html .= '                    </li>';
        }
    }

    $boxes->reset_postdata();

    $html .= '                </ul>';
    $html .= '            </div>';
    $html .= '        </div>';
    $html .= '    </div>';
    $html .= '</div>';
}

$html .= '</div></div>';

echo $html;

wp_reset_postdata(); // reset to the original page data
wp_reset_query(); // reset to the original page data