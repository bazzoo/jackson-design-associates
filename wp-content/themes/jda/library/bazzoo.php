<?php
/**
 * Created by PhpStorm.
 * User: wes
 * Date: 21/01/16
 * Time: 14:02
 */

//added by Wes to stop p tags being removed
remove_filter('the_content', 'wpautop');

//function insert_page_content_func($field = 'post_content', $id, $context = 'raw')
function bz_insert_page_shortcode($id)
{
    extract
    (
        shortcode_atts
        (
            array
            (
                'id' => 'id'
            ),
            $id
        )
    );

    return get_post_field('post_content', $id, 'raw');
}


function bz_insert_rev_slider_shortcode($id)
{
    extract
    (
        shortcode_atts
        (
            array
            (
                'id' => 'id'
            ),
            $id
        )
    );

    return get_post_field('post_title', $id, 'raw');
}


/**
 * Get taxonomies terms links.
 *
 * @see get_object_taxonomies()
 */
function wpdocs_custom_taxonomies_terms_links()
{
    $post = get_post( $post->ID );
    $post_type = $post->post_type;
    $taxonomies = get_object_taxonomies( $post_type, 'objects' );

    foreach ( $taxonomies as $taxonomy_slug => $taxonomy )
    {
        // Get the terms related to post.
        $terms = get_the_terms( $post->ID, $taxonomy_slug );

        if ( ! empty( $terms ) )
        {
            $out .= '<li class="title">' . $taxonomy_slug;

            if(sizeof($terms > 1))
            {
                $out .= 's';
            }

            $out .= '</li><li>';

            foreach ( $terms as $term )
            {
                $out .= $term->name . ', ';
            }

            $out = rtrim($out, ',');
        }
    }
    return $out;
}


// Creating a function to create our Project post Type
function add_projects_post_type()
{

    // Set UI labels for Projects Post Type
    $labels = array(
        'name' => _x('Projects', 'Post Type General Name', 'jda'),
        'singular_name' => _x('project', 'Post Type Singular Name', 'jda'),
        'menu_name' => __('Projects', 'jda'),
        'parent_item_colon' => __('Parent Project', 'jda'),
        'all_items' => __('All Projects', 'jda'),
        'view_item' => __('View Project', 'jda'),
        'add_new_item' => __('Add New Project', 'jda'),
        'add_new' => __('Add New', 'jda'),
        'edit_item' => __('Edit Project', 'jda'),
        'update_item' => __('Update Project', 'jda'),
        'search_items' => __('Search Project', 'jda'),
        'not_found' => __('Not Found', 'jda'),
        'not_found_in_trash' => __('Not found in Trash', 'jda'),
    );

    // Set other options for Projects Post Type
    $args = array(
        'label' => __('projects', 'jda'),
        'description' => __('JDA Projects', 'jda'),
        'labels' => $labels,
        // Features this CPT supports in Post Editor
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies' => array('discipline', 'sector'),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-welcome-write-blog',
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'register_meta_box_cb' => 'add_projects_metaboxes'
    );

    // Registering your Custom Post Type
    register_post_type('projects', $args);
}


// Creating a function to create our Person post Type
function add_people_post_type()
{

// Set UI labels for Custom Post Type
    $labels = array(
        'name' => _x('People', 'Post Type General Name', 'jda'),
        'singular_name' => _x('Person', 'Post Type Singular Name', 'jda'),
        'menu_name' => __('People', 'jda'),
        'parent_item_colon' => __('Parent Person', 'jda'),
        'all_items' => __('All People', 'jda'),
        'view_item' => __('View Person', 'jda'),
        'add_new_item' => __('Add New Person', 'jda'),
        'add_new' => __('Add New', 'jda'),
        'edit_item' => __('Edit Person', 'jda'),
        'update_item' => __('Update Person', 'jda'),
        'search_items' => __('Search Person', 'jda'),
        'not_found' => __('Not Found', 'jda'),
        'not_found_in_trash' => __('Not found in Trash', 'jda'),
    );

// Set other options for Custom Post Type

    $args = array(
        'label' => __('people', 'jda'),
        'description' => __('JDA People', 'jda'),
        'labels' => $labels,
        // Features this CPT supports in Post Editor
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-groups',
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'register_meta_box_cb' => 'add_people_metaboxes'
    );

    // Registering your Custom Post Type
    register_post_type('people', $args);
}


// Creating a function to create our discipline boxes post Type
function add_disciplineboxes_post_type()
{

// Set UI labels for Custom Post Type
    $labels = array(
        'name' => _x('Discipline Boxes', 'Post Type General Name', 'jda'),
        'singular_name' => _x('Discipline Box', 'Post Type Singular Name', 'jda'),
        'menu_name' => __('Discipline Boxes', 'jda'),
        'parent_item_colon' => __('Parent Person', 'jda'),
        'all_items' => __('All Discipline Boxes', 'jda'),
        'view_item' => __('View Discipline Box', 'jda'),
        'add_new_item' => __('Add New Discipline Box', 'jda'),
        'add_new' => __('Add New', 'jda'),
        'edit_item' => __('Edit Discipline Box', 'jda'),
        'update_item' => __('Update Discipline Box', 'jda'),
        'search_items' => __('Search Discipline Box', 'jda'),
        'not_found' => __('Not Found', 'jda'),
        'not_found_in_trash' => __('Not found in Trash', 'jda'),
    );

    // Set other options for Custom Post Type
    $args = array(
        'label' => __('discipline_boxes', 'jda'),
        'description' => __('Discipline Boxes', 'jda'),
        'labels' => $labels,
        // Features this CPT supports in Post Editor
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'revisions', 'disciplines', 'custom-fields'),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical' => true,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'can_export' => true,
        'has_archive' => false,
        'taxonomies' => array('disciplines'),
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page'
    );

    // Registering your Custom Post Type
    register_post_type('discipline_boxes', $args);
}


function discipline_init() {
    // create a new taxonomy
    register_taxonomy
    (
        'discipline',
        'project',
        array
        (
            // Hierarchical taxonomy (like categories)
            'hierarchical' => true,
            // This array of options controls the labels displayed in the WordPress Admin UI
            'labels' => array
            (
                'name' => _x( 'Project Discipline', 'taxonomy general name' ),
                'singular_name' => _x( 'Project Discipline', 'taxonomy singular name' ),
                'search_items' =>  __( 'Search Project Discipline' ),
                'all_items' => __( 'All Project Discipline' ),
                'edit_item' => __( 'Edit Project Discipline' ),
                'update_item' => __( 'Update Project Discipline' ),
                'add_new_item' => __( 'Add New Project Discipline' ),
                'new_item_name' => __( 'New  Project Discipline' ),
                'menu_name' => __( ' Project Discipline' ),
            ),

        // Control the slugs used for this taxonomy
            'rewrite' => array
            (
                'slug' => 'discipline', // This controls the base slug that will display before each term
                'with_front' => false, // Don't display the category base before "/locations/"
                'hierarchical' => false // This will allow URL's like "/locations/boston/cambridge/"
            ),
        )
    );

    register_taxonomy
    (
        'discipline',
        'discipline_boxes',
        array
        (
            // Hierarchical taxonomy (like categories)
            'hierarchical' => true,
            // This array of options controls the labels displayed in the WordPress Admin UI
            'labels' => array
            (
                'name' => _x( 'Project Discipline', 'taxonomy general name' ),
                'singular_name' => _x( 'Project Discipline', 'taxonomy singular name' ),
                'search_items' =>  __( 'Search Project Discipline' ),
                'all_items' => __( 'All Project Discipline' ),
                'edit_item' => __( 'Edit Project Discipline' ),
                'update_item' => __( 'Update Project Discipline' ),
                'add_new_item' => __( 'Add New Project Discipline' ),
                'new_item_name' => __( 'New  Project Discipline' ),
                'menu_name' => __( ' Project Discipline' ),
            ),

            // Control the slugs used for this taxonomy
            'rewrite' => array
            (
                'slug' => 'discipline', // This controls the base slug that will display before each term
                'with_front' => false, // Don't display the category base before "/locations/"
                'hierarchical' => false // This will allow URL's like "/locations/boston/cambridge/"
            ),
        )
    );
}


function sector_init() {
    // create a new taxonomy
    register_taxonomy
    (
        'sector',
        'project',
        array
        (
            // Hierarchical taxonomy (like categories)
            'hierarchical' => true,
            // This array of options controls the labels displayed in the WordPress Admin UI
            'labels' => array
            (
                'name' => _x( 'Project Sectors', 'taxonomy general name' ),
                'singular_name' => _x( 'Project Sector', 'taxonomy singular name' ),
                'search_items' =>  __( 'Search Project Sectors' ),
                'all_items' => __( 'All Project Sectors' ),
                'edit_item' => __( 'Edit Project Sector' ),
                'update_item' => __( 'Update Project Sector' ),
                'add_new_item' => __( 'Add New Project Sector' ),
                'new_item_name' => __( 'New Project Sector' ),
                'menu_name' => __( ' Project Sector' ),
            ),

            // Control the slugs used for this taxonomy
            'rewrite' => array(
                'slug' => 'sector', // This controls the base slug that will display before each term
                'with_front' => false, // Don't display the category base before "/locations/"
                'hierarchical' => false // This will allow URL's like "/locations/boston/cambridge/"
            ),
        ));
}


// Add the Projects Meta Boxes
function add_projects_metaboxes()
{
    add_meta_box
    (
        'wpt_project_client', // id
        'Client',             // title
        'wpt_project_client', // callback
        null,                 // screen
        'advanced',           // context
        'high',               // priority
        ''                    // callback args
    );

    add_meta_box
    (
        'wpt_project_size', // id
        'Size',             // title
        'wpt_project_size', // callback
        null,                 // screen
        'advanced',           // context
        'high',               // priority
        ''                    // callback args
    );
}


// Add the People Meta Boxes
function add_people_metaboxes()
{
    add_meta_box
    (
        'wpt_people_job_title', // id
        'Job Title',             // title
        'wpt_people_job_title', // callback
        null,                 // screen
        'advanced',           // context
        'high',               // priority
        ''                    // callback args
    );

    add_meta_box
    (
        'wpt_people_quals', // id
        'Qualifications',             // title
        'wpt_people_quals', // callback
        null,                 // screen
        'advanced',           // context
        'high',               // priority
        ''                    // callback args
    );

    add_meta_box
    (
        'wpt_people_order', // id
        'Sort Order',             // title
        'wpt_people_order', // callback
        null,                 // screen
        'advanced',           // context
        'high',               // priority
        ''                    // callback args
    );

    add_meta_box
    (
        'wpt_people_discipline', // id
        'Discipline',             // title
        'wpt_people_discipline', // callback
        null,                 // screen
        'advanced',           // context
        'high',               // priority
        ''                    // callback args
    );
}


// Add the Discipline Boxes Meta Boxes
function add_discipline_boxes_metaboxes()
{
    add_meta_box
    (
        'wpt_discipline_boxes_order', // id
        'Order',             // title
        'wpt_discipline_boxes_order', // callback
        null,                 // screen
        'advanced',           // context
        'high',               // priority
        ''                    // callback args
    );
}


function wpt_project_client()
{
    global $post;

    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="projectmeta_noncename" id="projectmeta_noncename" value="' .
        wp_create_nonce(__FILE__) . '" />';

    // Get the client data if its already been entered
    $client = get_post_meta($post->ID, '_client', true);

    // Echo out the field
    echo '<input type="text" name="_client" value="' . $client . '" class="widefat" required/>';
}
function wpt_project_size()
{
    global $post;

    // Get the size data if its already been entered
    $size = get_post_meta($post->ID, '_size', true);
    ?>
    <div style="float:left; margin: 0 2vw">
        <table style="">
            <tr>add_people_metaboxes
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw;">
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw;">
            </tr>
            <tr>
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw; background: #444";>
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw;">
            </tr>
        </table>
        <input type="radio" name="_size" value="small" <?php checked( $size, 'small' ); ?> required">Small</input>
    </div>
    <div style="float:left; margin: 0 2vw">
        <table>
            <tr>
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw; background: #444">
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw; background: #444"">
            </tr>
            <tr>
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw;">
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw;">
            </tr>
        </table>
        <input type="radio" name="_size" value="portrait" <?php checked( $size, 'portrait' ); ?>" required>Portrait</input>
    </div>
    <div style="float:left;margin: 0 2vw">
        <table>
            <tr>
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw; background: #444"">
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw; background: #444">
            </tr>
            <tr>
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw; background: #444">
                <td style="border: dotted 1px #999; height: 3vw; width: 3vw; background: #444">
            </tr>
        </table>
        <input type="radio" name="_size" value="large" <?php checked( $size, 'large' ); ?>" required>Large</input>
    </div>


    <?php
}


function wpt_people_job_title()
{
    global $post;

    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="projectmeta_noncename" id="projectmeta_noncename" value="' .
        wp_create_nonce(__FILE__) . '" />';

    // Get the client data if its already been entered
    $client = get_post_meta($post->ID, '_job_title', true);

    // Echo out the field
    echo '<input type="text" name="_job_title" value="' . $client . '" class="widefat" required/>';
}
function wpt_people_quals()
{
    global $post;

    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="projectmeta_noncename" id="projectmeta_noncename" value="' .
        wp_create_nonce(__FILE__) . '" />';

    // Get the client data if its already been entered
    $client = get_post_meta($post->ID, '_quals', true);

    // Echo out the field
    echo '<input type="text" name="_quals" value="' . $client . '" class="widefat" required/>';
}
function wpt_people_order()
{
    global $post;

    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="projectmeta_noncename" id="projectmeta_noncename" value="' .
        wp_create_nonce(__FILE__) . '" />';

    // Get the client data if its already been entered
    $client = get_post_meta($post->ID, '_order', true);

    // Echo out the field
    echo '<input type="text" name="_order" value="' . $client . '" class="widefat" required/>';
}

function wpt_people_discipline()
{
    global $post;

    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="peoplemeta_noncename" id="peoplemeta_noncename" value="' .
        wp_create_nonce(__FILE__) . '" />';

    // Get the client data if its already been entered
    $discipline = get_post_meta($post->ID, '_discipline', true);

    // Echo out the field
    echo '<input type="text" name="_discipline" value="' . $discipline . '" class="widefat" required/>';
}


function wpt_discipline_boxes_order()
{
    global $post;

    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="discipline_boxesmeta_noncename" id="discipline_boxesmeta_noncename" value="' .
        wp_create_nonce(__FILE__) . '" />';

    // Get the client data if its already been entered
    $order = get_post_meta($post->ID, '_order', true);

    // Echo out the field
    echo '<input type="text" name="_order" value="' . $order . '" class="widefat" required/>';
}


function add_projects_post_types_to_loop( $query )
{
    if ( is_home() && $query->is_main_query() )

        $query->set( 'post_type', array( 'post', 'project' ) );

    return $query;
}


function wpt_save_projects_meta($post_id, $post)
{
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if (!wp_verify_nonce($_POST['projectmeta_noncename'], __FILE__))
    {
        return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if (!current_user_can('edit_post', $post->ID))
        return $post->ID;

    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.
    $projects_meta['_client'] = $_POST['_client'];
    $projects_meta['_size'] = $_POST['_size'];

    // Add values of $projects_meta as custom fields
    foreach ($projects_meta as $key => $value)
    {
        // Cycle through the $projects_meta array!
        if ($post->post_type == 'revision')
        {
            return; // Don't store custom data twice
        }

        $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)

        if (get_post_meta($post->ID, $key, FALSE))  // If the custom field already has a value
        {
            update_post_meta($post->ID, $key, $value);
        }

        else // If the custom field doesn't have a value
        {
            add_post_meta($post->ID, $key, $value);
        }
        if (!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }
}
function wpt_save_people_meta($post_id, $post)
{
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if (!wp_verify_nonce($_POST['peoplemeta_noncename'], __FILE__))
    {
        return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if (!current_user_can('edit_post', $post->ID))
        return $post->ID;

    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.
    $people_meta['_job_title'] = $_POST['_job_title'];
    $people_meta['_quals'] = $_POST['_quals'];
    $people_meta['_order'] = $_POST['_order'];
    $people_meta['_discipline'] = $_POST['_discipline'];

    // Add values of $projects_meta as custom fields
    foreach ($people_meta as $key => $value)
    {
        // Cycle through the $projects_meta array!
        if ($post->post_type == 'revision')
        {
            return; // Don't store custom data twice
        }

        $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)

        if (get_post_meta($post->ID, $key, FALSE))  // If the custom field already has a value
        {
            update_post_meta($post->ID, $key, $value);
        }

        else // If the custom field doesn't have a value
        {
            add_post_meta($post->ID, $key, $value);
        }
        if (!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }
}
function wpt_save_discipline_boxes_meta($post_id, $post)
{
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if (!wp_verify_nonce($_POST['discipline_boxesmeta_noncename'], __FILE__))
    {
        return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if (!current_user_can('edit_post', $post->ID))
        return $post->ID;

    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.

    $discipline_boxes_meta['_order'] = $_POST['_order'];

    // Add values of $projects_meta as custom fields
    foreach ($discipline_boxes_meta as $key => $value)
    {
        // Cycle through the $projects_meta array!
        if ($post->post_type == 'revision')
        {
            return; // Don't store custom data twice
        }

        $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)

        if (get_post_meta($post->ID, $key, FALSE))  // If the custom field already has a value
        {
            update_post_meta($post->ID, $key, $value);
        }

        else // If the custom field doesn't have a value
        {
            add_post_meta($post->ID, $key, $value);
        }
        if (!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }
}






function bz_admin_theme()  //makes image size tables work
{
    wp_enqueue_style('bz-admin-theme', get_stylesheet_uri(), __FILE__);
}

/*******************************************************************************/
/*******************************************************************************/

//Add custom themes
add_action('admin_enqueue_scripts', 'bz_admin_theme');

//Add all cusom post types
add_action( 'init', 'add_projects_post_type', 2, 0);
add_action( 'init', 'add_people_post_type', 2, 0);
add_action( 'init', 'add_disciplineboxes_post_type', 2, 0);

//Add all custom taxonomies
add_action( 'init', 'discipline_init', 1, 0);
add_action( 'init', 'sector_init', 1, 0);

//Add action to save post types META
add_action('save_post', 'wpt_save_projects_meta', 1, 2); // save the custom fields
add_action('save_post', 'wpt_save_people_meta', 1, 2); // save the custom fields
add_action('save_post', 'wpt_save_discipline_boxes_meta', 1, 2); // save the custom fields

//Add custom Shortcodes
add_shortcode('bz_insert_page_content', 'bz_insert_page_shortcode');
add_shortcode('bz_insert_rev_slider', 'bz_insert_rev_slider_shortcode');


add_action( 'pre_get_posts', 'add_projects_post_types_to_loop' );


remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );