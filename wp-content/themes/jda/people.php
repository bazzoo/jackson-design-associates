<?php
/**
 * Template Name: People view
 **/
?>

<?php get_header(); ?>

    <?php

        $args = array();
        $args['post_type']      = 'people';
        $args['post_status']    = 'publish';
        $args['posts_per_page'] = '6';
        $args['orderby']        = 'ID';
        $args['order']          = 'asc';

        $meta = get_post_meta('job-title');
        $loop = new WP_Query( $args );

        $html = '<div class="modals people">';
        foreach($loop->posts as $post)
        {
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
            $meta = get_post_meta($post->ID);

            $html .= '<div id="' . $post->post_name . '-modal" class="reveal-modal" data-reveal aria-hidden="true" role="dialog">';
            $html .= '    <div class="content wow fadeIn ' . $post->post_name . '">';
            $html .= '        <div class="stats">';
            $html .= '            <h4 class="wow fadeInUp">' . $post->post_title . '</h4>';
            $html .= '            <ul class="wow fadeInUp">';
            $html .= '                <li>' . $meta['_job_title'][0] . '</li>';
            $html .= '                <li>' . $meta['_quals'][0] . '</li>';
            $html .= '                <li>' . $meta['_discipline'][0] . '</li>';
            $html .= '            </ul>';
            $html .= '        </div>';
            $html .= $post->post_content;
            $html .= '        <a class="close-reveal-modal wow fadeInRight" aria-label="Close"><button>&#215;</button></a>';
            $html .= '    </div>';
            $html .= '</div>';
        }

        $html .= '</div>';
        $html .= '<div class="row people-container">';

        foreach($loop->posts as $post)
        {
            $name = $post->post_title;
            $html .= '<a href="#" data-reveal-id="' . $post->post_name . '-modal">';
            $html .= '    <div class="large-4 columns ' . $post->post_name . '"">';
            $html .= '        <h2>' . $post->post_title . '</h2>';
            $html .= '        <p>' . $meta['_job_title'][0] .'</p>';
            $html .= '    </div>';
            $html .= '</a>';
        }
        $html .= '</div>';
        echo $html;
    ?>

<?php get_footer(); ?>